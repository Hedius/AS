import pandas
import numpy as np
from matplotlib import patches, pyplot as plt, transforms
from matplotlib.patches import Ellipse
from scipy.spatial.distance import cdist
from scipy.stats import norm, chi, chi2

csv = pandas.read_csv('../wine-data_reduced.csv', sep=' ')
data = csv.values

x = data.T[0]
y = data.T[1]

# pfusch
arr = []
for _ in range(100):
    arr.append(norm(0, np.sqrt(2)).rvs(3))
data = np.array(arr)

x = data.T[0]
y = data.T[1]

# a
center_point = np.mean(data, 0)
distance = cdist(data, [center_point], 'mahalanobis')
for i in range(1, 4):
    n = 0
    for val in distance:
        if val[0] <= i:
            n += 1
    print(f'Points in {i}-sigma distance: {n}')

# b
x_std = x.std()
y_std = y.std()
x_mean = x.mean()
y_mean = y.mean()

for i in range(1, 4):
    n = 0
    for val in data:
        if val[0] > (x_mean + i * x_std):
            continue
        if val[0] < (x_mean - i * x_std):
            continue
        if val[1] > (y_mean + i * y_std):
            continue
        if val[1] < (y_mean - i * y_std):
            continue
        n += 1
    print(f'B: {i}-sigma: {n}')


# Plot (c)
# Source of this function:
# https://matplotlib.org/devdocs/gallery/statistics/confidence_ellipse.html
def confidence_ellipse(x, y, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of *x* and *y*.

    Parameters
    ----------
    x, y : array-like, shape (n, )
        Input data.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    **kwargs
        Forwarded to `~matplotlib.patches.Ellipse`

    Returns
    -------
    matplotlib.patches.Ellipse
    """
    if x.size != y.size:
        raise ValueError("x and y must be the same size")

    cov = np.cov(x, y)
    pearson = cov[0, 1]/np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensional dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0), width=ell_radius_x * 2, height=ell_radius_y * 2,
                      facecolor=facecolor, **kwargs)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = np.sqrt(cov[0, 0]) * n_std
    mean_x = np.mean(x)

    # calculating the stdandard deviation of y ...
    scale_y = np.sqrt(cov[1, 1]) * n_std
    mean_y = np.mean(y)

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)


plt.title('3')
ax = plt.subplot(2, 1, 1)
ax.set(title='Confidence Ellipsoid', xlabel='x', ylabel='y')
confidence_ellipse(x, y, ax, 3, 'grey')
confidence_ellipse(x, y, ax, 2, 'red')
confidence_ellipse(x, y, ax, 1, 'green')
plt.scatter(data[:, 0], data[:, 1])

# d ?
cov = np.cov(data.T)
eig, _ = np.linalg.eig(cov)

ax = plt.subplot(2, 1, 2)
ax.set(title='Normalized', xlabel='x', ylabel='y')
plt.scatter(data[:, 0]/x_std, data[:, 1]/y_std)
plt.show()
