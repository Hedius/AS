import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.decomposition import PCA

wine = datasets.load_wine()
data = wine.data
n = data.shape[0]


def pca_own():
    # mean of each column
    x_mean = np.mean(data, 0)
    x_std = np.std(data, 0)

    # standardize data by subtracting mean and dividing by std
    # multiplikation mit np.ones braucht es nicht
    x_norm = (data - x_mean * np.ones(data.shape))  # / x_std
    print(f'Std of normalized data is: {np.std(x_norm, 0)}')

    # covariance matrix
    # c = 1 / (n - 1) * x_norm.transpose().dot(x_norm)
    # or easier:
    c = np.cov(x_norm, rowvar=False)
    if np.linalg.eigvals(c).all() > 0:
        print('covariance matrix is pos. definite.')
    else:
        ValueError('Cannot continue!')

    # eig not sorted
    eiv, eig = np.linalg.eig(c)

    # sort eig
    # argsort -> indexes sorted
    sorted_index = np.argsort(eiv)[::-1]
    eiv = eiv[sorted_index]
    eig = eig[:, sorted_index]

    # svd
    # U = V
    # S = EIGENVALUES (sorted desc)
    # U, V, eigenvectors
    # u, s, v = np.linalg.svd(c)

    n_components = 2
    t = eig[:, 0:n_components]
    return x_norm.dot(t)


def plot_2(x_pca, plot=1, title='PCA plot for Wine Data'):
    ax = plt.subplot(2, 1, plot)
    ax.set(xlabel='PC1', ylabel='PC2')
    for i in range(n):
        wine_class = wine.target[i]
        x = x_pca.T[0][i]
        y = x_pca.T[1][i]

        if wine_class == 0:
            color = 'red'
        elif wine_class == 1:
            color = 'blue'
        else:
            color = 'black'
        # problem: ich plotte immer nur einen punkt hmmm
        plt.scatter(x=x, y=y, c=color, label=wine.target_names[wine_class])
    plt.title(title)
    # plt.legend()


def pca():
    pca = PCA(n_components=2)  # creates an instance of PCA class
    results = pca.fit(data)  # applies PCA on predictor variables
    x_pca = results.transform(data)  # create a new array of latent variables
    return x_pca
    

# own implementation
x_pca = pca_own()
plot_2(x_pca)

# pca lib
x_pca = pca()
plot_2(x_pca, plot=2, title='Probe mit lib')

plt.show()
