import numpy as np
import scipy.stats as stats
import pandas
import matplotlib.pyplot as plt

csv = pandas.read_csv('../data_NeuroEnhancement.txt', sep='\t', header=None)

data = csv.to_numpy()

alpha = 0.01

"""
H0: The performance did not improve.
H1 The performance did improve.
"""

before = data[:, 1]
after = data[:, 2]

plt.hist(before, bins=20)
plt.figure()
plt.hist(after, bins=20)
plt.show()

if stats.shapiro(before).pvalue < alpha:
    print('Before values are not normal distributed.')
else:
    print('Before values are normal distributed.')

if stats.shapiro(after).pvalue < alpha:
    print('After values are not normal distributed.')
else:
    print('After values are normal distributed.')


print(f'STD: before={np.std(before)} after={np.std(after)}')

# test = stats.ranksums(before, after)
# x < y statistically less
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ranksums.html
"""
‘less’: the distribution underlying x is stochastically less than the distribution underlying y.

S. 30 3. test
t-test for paired observations.
"""
# test = stats.ranksums(before, after, alternative='less')
test = stats.wilcoxon(before, after, alternative='less')
# std not matching
# test = stats.ttest_rel(before, after, alternative='less')

if test.pvalue < alpha:
    print('The performance did improve.')
else:
    print('The performance did not improve.')

print(csv.describe())
