import numpy as np
import scipy.stats as stats
import pandas
import matplotlib.pyplot as plt
from statsmodels.stats.descriptivestats import sign_test

csv = pandas.read_csv('../data_AnnualDemand.txt')

data = csv.to_numpy()

alpha = 0.01

prev_avg = 1140

"""
H0: The data matches the given metric.
H1: The data does not match the given metric.

Wilcoxon Signed Rank Test
"""


if stats.shapiro(data).pvalue < alpha:
    print('values are not normal distributed.')
else:
    print('values are normal distributed.')

# plt.hist(data, bins=100)
# plt.show()

norm_data = data - prev_avg

_, pvalue = sign_test(data, prev_avg)

print(f'pvalue={pvalue}')

if pvalue < alpha:
    print('The demand did change.')
else:
    print('The demand did not change.')
