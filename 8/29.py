import numpy as np
import scipy.stats as stats
import pandas
import matplotlib.pyplot as plt


def check(val):
    if val:
        print('The mean is not 300. (5% significance)')
    else:
        print('The mean is 300. (5% significance)')


csv = pandas.read_csv('../data_MolecularMovements.txt', sep='\t', header=None)

data = csv.to_numpy()

alpha = 0.05

wanted_mean = 300

"""
H0: Follow a normal distribution
H1 Do not follow a normal dist

H0: Expected mean value is 300
H1 expected value != 300
"""

# plt.hist(data, bins=20)
# plt.show()

if stats.shapiro(data).pvalue < alpha:
    print('The data does not follow a normal distribution.')
else:
    print('The data does follow a normal distribution.')


# Eigene implementierung
n = len(data)
mean = np.mean(data)
std = np.std(data, ddof=1)
t = np.sqrt(n) * (mean - wanted_mean) / std


t_abs = np.abs(t)

rule = stats.t.ppf(1 - alpha / 2, n - 1)
p_value = 2 - 2 * stats.t.cdf(t_abs, n - 1)

print(
    f"""\
Own implementation:
    t={t}
    pvalue={p_value}
"""
)
check(t_abs >= rule)

# Probe
test = stats.ttest_1samp(data, wanted_mean)
print(
    f"""\
Probe: {test}
"""
)
check(np.abs(test.statistic) >= rule)
check(p_value < alpha)

print(csv.describe())

