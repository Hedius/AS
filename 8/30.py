import numpy as np
import scipy.stats as stats
from scipy.special import binom
import pandas
import matplotlib.pyplot as plt

n = 2

n_balls = 100

alpha = 0.05

"""
H0: the board is not loaded and the distribution is 0.25 0.5 0.25
H1: the board is loaded.

Significance: 5%
"""


def dir(p=0.5):
    return np.random.rand() > p


def fall():
    first_right = dir(0.6)
    second_right = dir(0.5)
    if first_right and second_right:
        return 2
    elif not first_right and not second_right:
        return 0
    else:
        return 1


def simu():
    containers = [0, 0, 0]
    for _ in range(n_balls):
        containers[fall()] += 1
    return containers


containers = simu()
print(f'Measured / simulated frequency: {containers}')

for i in range(3):
    e = n_balls * binom(2, i) * np.power(0.5, i) * np.power(0.5, 2 - i)
    print(f'E[B{i}] = {e}')

p_vals = [0.25, 0.5, 0.25]

frequency_fair = [n_balls * 0.25, n_balls * 0.5, n_balls * 0.25]
print(f'Fair frequency: {frequency_fair}')


results = []
for i in range(1000):
    containers = simu()
    test = stats.chisquare(containers, frequency_fair)
    results.append(test.pvalue < alpha)
print(test)

if test.pvalue < alpha:
    print('The galton board is loaded.')
else:
    print('The galton board is not loaded.')

print(f'Rejected {results.count(True)}')

"""
ToDo: Decision rule nicht immer anwendbar
"""