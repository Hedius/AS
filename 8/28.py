import numpy as np
import scipy.stats as stats
import pandas
import matplotlib.pyplot as plt

csv = pandas.read_csv('../data_FineParticles.txt', sep='\t', header=None)

data = csv.to_numpy()

alphas = [0.05, 0.01]

"""
H0: The data is not the same.
H1 The data is the same.
"""

groedig = data[:, 0]
golling = data[:, 1]

plt.hist(groedig, bins=20)
plt.figure()
plt.hist(golling, bins=20)
plt.show()

for val in [groedig, golling]:
    if stats.shapiro(val).pvalue < 0.05:
        print('values are not normal distributed.')
    else:
        print('values are normal distributed.')

# test = stats.ranksums(before, after)
# x < y statistically less
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ranksums.html
"""
‘less’: the distribution underlying x is stochastically less than the distribution underlying y.

Mann-Whitnes U-test (ranksums) for unpaired data.
"""
test = stats.ranksums(groedig, golling, alternative='two-sided')

for alpha in alphas:
    print(f'Alpha: {alpha}')
    if test.pvalue < alpha:
        print('The data is not same for both locations.')
    else:
        print('The data is the same for both locations.')

print(csv.describe())
print(test.pvalue)
