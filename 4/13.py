import numpy as np
from scipy import stats

dist = stats.norm(2, np.sqrt(3))


def main():
    sizes = [100, 1000, 10000]
    for size in sizes:
        sample = dist.rvs(size)

        mean = sample.mean()
        var = sample.var()

        print(f'n: {size}, Mean: {mean}, Var: {var}')


if __name__ == '__main__':
    main()
