from typing import Dict

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import multiprocessing

mean = 16
std = 2
p_choose = [0.25, 0.35, 0.4]

N = 10000

dist_choose = stats.multinomial(n=1, p=p_choose)
dist_customers = stats.norm(mean, std)


def choose_car(available_cars: Dict):
    chosen = dist_choose.rvs(1)[0]
    wanted_car = int(np.where(chosen == 1)[0]) + 1

    # car available?
    if available_cars[wanted_car] == 0:
        return False

    # choose car
    available_cars[wanted_car] -= 1
    return True


def simu(_=None):
    cars = {
        1: 5,
        2: 6,
        3: 6
    }
    n_customers = int(dist_customers.rvs(1)[0].round())
    not_fulfilled = 0
    for _ in range(n_customers):
        if not choose_car(cars):
            not_fulfilled += 1
    return not_fulfilled


def control(n):
    with multiprocessing.Pool(8) as p:
        sample = p.map(simu, [_ for _ in range(n)])
    values, counts = np.unique(sample, return_counts=True)

    print(f'Tries: {n}\n\n')
    for value, count in zip(values, counts):
        p = count / np.sum(counts)
        print(f'{value}: p = {p}, n = {count}')
    plt.hist(sample)


control(N)
plt.show()
