from typing import Dict

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import multiprocessing

m = 20

dist = stats.randint(1, m + 1)

# Würfeln pro wiederholung
ns = [1, 5, 10, 25, 50, 75, 100, 500, 1000, 10000]
# Wiederholungen
N = 500


def choose(n):
    return dist.rvs(n)


def estimate_m(numbers):
    return 2 * numbers.mean() - 1


def simu(n):
    numbers = choose(n)
    estimated_m = estimate_m(numbers)
    return estimated_m


def control():
    results = []
    for n in ns:
        cur_result = []
        for _ in range(N):
            estimation = simu(n)
            cur_result.append(estimation)
        print(f'Simulations for n: {n}: Min: {np.min(cur_result)} Max: {np.max(cur_result)} '
              f'Mean: {np.mean(cur_result)}, '
              f'Med: {np.median(cur_result)}')
        results.append(cur_result)

    plt.boxplot(results, labels=[f'n={n}' for n in ns])


def c():
    drawn_numbers = np.asarray([34, 56, 17, 22, 23, 88])
    estimated_m = estimate_m(drawn_numbers)
    print(f'Estimated m for c: {estimated_m}')


c()
control()
plt.show()
