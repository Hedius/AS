from typing import Dict

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import multiprocessing

costs = 3
price = 7
price_old = 5

p_demand = [0.01, 0.02, 0.03, 0.04, 0.05, 0.1, 0.2, 0.2, 0.1, 0.09, 0.08,
            0.07, 0.01]

bought_flowers = [5, 10]

alpha = 0.01

dist_demand = stats.multinomial(n=1, p=p_demand)


cold_storage = 0


def demand():
    return int(np.where(dist_demand.rvs(1)==1)[1])


def win(needed, stock):
    _costs = costs * stock
    income = price * (needed if needed <= stock else stock)
    return income - _costs


def win_cold(needed, stock):
    global cold_storage
    _costs = costs * stock
    income = 0
    for _ in range(needed):
        if stock >= 1:
            income += price
            stock -= 1
        elif cold_storage >= 1:
            income += price_old
            cold_storage -= 1
        else:
            break
    cold_storage = stock
    return income - _costs


def simu(n, cold=False):
    _demand = demand()
    if cold:
        profit = win_cold(_demand, n)
    else:
        profit = win(_demand, n)
    return profit


def control(bought, n):
    global cold_storage
    cold_storage = 0
    before = [simu(bought) for _ in range(n)]
    after = [simu(bought, cold=True) for _ in range(n)]

    # if stats.shapiro(before).pvalue < alpha:
    #     print(f'{bought}: Before values are not normal distributed.')
    # else:
    #     print(f'{bought}: Before values are normal distributed.')

    # if stats.shapiro(after).pvalue < alpha:
    #     print(f'{bought}: After values are not normal distributed.')
    # else:
    #     print(f'{bought}: After values are normal distributed.')

    """
    ‘less’: the distribution underlying x is stochastically less than the distribution underlying y.

    Mann-Whitnes U-test (ranksums) for unpaired data.
    """
    test = stats.ranksums(after, before, alternative='greater')

    if test.pvalue < alpha:
        print(f'median: {np.median(before)} mean: {np.mean(before)}')
        print(f'median: {np.median(after)} mean: {np.mean(after)}')
        print(f'The data median did improve at {n}.')
        return True
    else:
        return False



if __name__ == '__main__':
    N = 1000
    for i in bought_flowers:
        for n in range(100, 10000):
            if control(i, n):
                break
