from typing import Dict

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import multiprocessing

costs = 3
price = 7

p_demand = [0.01, 0.02, 0.03, 0.04, 0.05, 0.1, 0.2, 0.2, 0.1, 0.09, 0.08,
            0.07, 0.01]

N = 365
max_flowers = 20

dist_demand = stats.multinomial(n=1, p=p_demand)


def demand():
    return int(np.where(dist_demand.rvs(1)==1)[1])


def win(needed, stock):
    _costs = costs * stock
    income = price * (needed if needed <= stock else stock)
    return income - _costs


def simu():
    _demand = demand()
    profits = [win(_demand, stock=i) for i in range(max_flowers)]
    return profits


def control(n):
    sample = np.asarray([simu() for _ in range(n)])
    # sum of column if we buy x flowers
    sums = sample.sum(axis=0)
    plt.bar([i for i in range(max_flowers)], sums)
    plt.xlabel('Bought Flowers')
    plt.ylabel('Profit')
    plt.locator_params(axis='both', integer=True, tight=True)
    return np.mean(sample)


print(control(N))
