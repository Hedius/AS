from typing import Dict

import matplotlib.pyplot as plt
import numpy as np
import scipy
import scipy.stats as stats
import multiprocessing

costs = 3
price = 7

p_demand = [0.01, 0.02, 0.03, 0.04, 0.05, 0.1, 0.2, 0.2, 0.1, 0.09, 0.08,
            0.07, 0.01]

N = 30000
bought_flowers = 7

dist_demand = stats.multinomial(n=1, p=p_demand)

alpha = 0.01


def demand():
    return int(np.where(dist_demand.rvs(1)==1)[1])


def win(needed, stock):
    _costs = costs * stock
    income = price * (needed if needed <= stock else stock)
    return income - _costs


def simu():
    _demand = demand()
    return win(_demand, bought_flowers)




def control():
    means = []
    samples = []
    lower_ci = []
    upper_ci = []
    for n in range(10, N):
        samples.append(simu())
        mean = np.mean(samples)
        means.append(mean)
        std = np.std(samples, ddof=1)
        k = scipy.stats.t.ppf(1 - alpha / 2, n - 1)

        l = k * std / np.sqrt(n)
        lower_ci.append(mean - l)
        upper_ci.append(mean + l)

        if l / mean <= 0.01:
            print(f'{n}: Confidence interval below 1 %')
            break


    plt.plot(means)
    plt.plot(lower_ci)
    plt.plot(upper_ci)

    # sum of column if we buy x flowers
    # plt.bar([i for i in range(max_flowers)], sums)
    plt.xlabel('Simulated days')
    plt.ylabel('Mean')
    plt.ylim(10, 30)
    plt.locator_params(axis='both', integer=True, tight=True)


print(control())
