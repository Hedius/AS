import numpy as np
from scipy.stats import pareto
import matplotlib.pyplot as plt

values = [30, 100, 120, 250, 57]
x_min = 30

mean = np.mean(values)

estimated_k = mean / (mean - x_min)

print(f'k: {estimated_k}')

x = np.linspace(x_min, 400, 200)

plt.plot(x, pareto.pdf(x, estimated_k))
plt.scatter(values, pareto(estimated_k).pdf(values))
plt.vlines(values, ymin=0, ymax=pareto(estimated_k).pdf(values))
plt.show()
