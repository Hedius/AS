import numpy as np
from scipy.stats import expon, randint
import matplotlib.pyplot as plt

from dataViews import TabularData

sample_size = 100

values = [10, 20, 40, 80]

m = 500

# probleme -> 10 fische ziehen, dann anschauen -> dann zurücklegen
# estimator undefiniert, wenn t = 0 (fall erfassen)



def fish():
    return randint.rvs(low=0, high=m)


def simu(c, r):
    marked = []
    # fish and marked
    for _ in range(0, c):
        i = fish()
        if i in marked:
            # already fished. fish again
            continue
        marked.append(i)

    # fish and check if marked
    t = 0
    for _ in range(0, r):
        i = fish()
        if i in marked:
            t += 1
    return t


def control(c, r):
    results = [simu(c, r) for _ in range(sample_size)]
    return results


def main():
    view = TabularData()
    view.set_columns(['c', 'r', 'mean', 'std', 'estimation'])
    for c in values:
        for r in values:
            results = control(c, r)
            view.add_row([c, r, np.mean(results), np.std(results), c * r / np.mean(results)])
    print(view.render())


if __name__ == '__main__':
    main()
