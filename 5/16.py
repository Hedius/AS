import numpy as np
from scipy.stats import expon
import matplotlib.pyplot as plt

sample_size = 10000
tau = 10


def simu(x):
    return (1 / tau) * np.exp(-x/tau)


values = []
for i in range(0, sample_size):
    values.append(simu(i))

print(np.mean(values))
