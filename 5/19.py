import numpy as np
from scipy.stats import t, norm
import matplotlib.pyplot as plt
import seaborn

x = np.linspace(-10, 10, 1000)


def simulation(n):
    numbers = norm.rvs(size=3)
    mean = np.mean(numbers)
    var = np.var(numbers, ddof=1)
    s = mean / (np.sqrt(var) / np.sqrt(3))
    return s


def control(n):
    data = [simulation(n) for _ in range(1000)]
    return data


N = [1, 2, 3, 5, 10]
plt.plot(x, norm.pdf(x), label='PDF: norm')
for n in N:
    plt.plot(x, t.pdf(x, df=n), label=f'PDF: t, N={n}')
plt.legend()

for n in N:
    plt.figure()
    plt.xlim([-10, 10])
    plt.plot(x, norm.cdf(x), label='CDF: norm')
    plt.plot(x, t.cdf(x, df=n), label=f'CDF: t, N={n}')

    # ECDF plot simulation
    s = np.array(control(n))
    plt.plot(np.sort(s), np.linspace(0, 1, len(s), endpoint=False), label=f'ECDF: N={n}')
    plt.legend()

plt.show()
