import pandas
import numpy as np
import scipy.stats
from matplotlib import pyplot as plt
from scipy.stats import norm
from KDEpy import FFTKDE

csv = pandas.read_csv('../data_KDE_1D.csv')
data = csv.to_numpy()

print(csv.describe())

# b
# plt.scatter(x=data[:, 0], y=0 * np.ones(len(data)))
# plt.show()
groups = 20
ax = plt.subplot(1, 1, 1)
plt.hist(data, groups, density=True, label='Histogram', edgecolor='blue',
         color='w')

# c
x = np.linspace(np.min(data) - 5, np.max(data) + 5, num=2**10)
ax.set(title='Normal Distribution')
loc, scale = scipy.stats.norm.fit(data)
plt.plot(x, norm.pdf(x, loc, scale), color='r', ls='--', label='PDF')
plt.legend(loc='best')

# d
kernels = {
    'gaussian': {
        'color': 'r'
    },
    'box': {
        'color': 'g'
    },
    'tri': {
        'color': 'orange',
    }
}

variations = [
    {'bw': 0.25},
    {'bw': 0.5},
    {'bw': 1},
    {'bw': 'silverman'},
    {'bw': 'ISJ'},
    {'bw': 'scott'}
]

plt.figure()
rows = int(np.ceil(np.sqrt(len(variations) / 2)))
columns = 2 * rows
for i, variation in enumerate(variations):
    ax = plt.subplot(rows, columns, i + 1)
    ax.set(title=f'KDE: BW: {variation["bw"]}')
    plt.hist(data, groups, density=True, label='Histogram', edgecolor='blue',
             color='w')
    for kernel in kernels:
        kde_x, kde_y = FFTKDE(kernel=kernel,
                              bw=variation['bw']).fit(data).evaluate()
        plt.plot(kde_x, kde_y, color=kernels[kernel]['color'],  # ls='--',
                 label=f'PDF: {kernel}')
        plt.legend(loc='best')

weights = [
    {'name': 'exp * 25', 'weights': np.exp(data) * 25},
    {'name': '^2 * 25', 'weights': data**2 * 25},
    {'name': '/ exp * 10', 'weights': np.exp(1 / data) * 10},
]


plt.figure()
rows = int(np.ceil(np.sqrt(len(weights) / 2)))
columns = 2 * rows
for i, weight in enumerate(weights):
    ax = plt.subplot(rows, columns, i + 1)
    ax.set(title=f'KDE: Weight: {weight["name"]}')
    plt.hist(data, groups, density=True, label='Histogram', edgecolor='blue',
             color='w')
    kde_x, kde_y = FFTKDE(kernel='gaussian',
                          bw=0.5).fit(data, weights=weight['weights']).evaluate()
    plt.plot(kde_x, kde_y, color='r',  # ls='--',
             label='Weighted PDF')
    plt.legend(loc='best')

plt.show()
