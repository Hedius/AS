import pandas
import numpy as np
import scipy.stats
from matplotlib import pyplot as plt
from scipy.stats import norm, multivariate_normal
from KDEpy import FFTKDE

csv = pandas.read_csv('../data_KDE_2D.csv')
data = csv.to_numpy()

mean = data.mean(0)
std = data.std()
print(csv.describe())

x = data[:, 0]
y = data[:, 1]
# b
ax = plt.subplot(2, 2, 1)
plt.scatter(x=x, y=y)
groups = 20
ax = plt.subplot(2, 2, 2)

try:
    plt.hist2d(x, y, groups)
except:
    pass

# c
plt.figure()
ax = plt.subplot(1, 2, 1, projection='3d')
ax.set(title='Normal Distribution')
mean = np.mean(data, 0)
cov = np.cov(data.T)
_x = _y = np.arange(-5, 10, 0.1)
X, Y = np.meshgrid(_x, _y)
comb = np.dstack((X, Y))
p = multivariate_normal.pdf(comb, mean=mean, cov=cov)
ax.plot_surface(X, Y, p, alpha=0.6, color='yellow')
ax.scatter(xs=x, ys=y, zs=0.01)
ax.set(xlabel='x', ylabel='y', zlabel='z')

ax = plt.subplot(1, 2, 2)
ax.scatter(x=x, y=y)
ax.contour(X, Y, p)
ax.set(xlabel='x', ylabel='y', title='Contour')

# d
kernels = {
    'gaussian': {
        'color': 'r'
    },
    'box': {
        'color': 'g'
    },
    'tri': {
        'color': 'orange',
    }
}

variations = [
    {'bw': 0.25},
    {'bw': 0.5},
    {'bw': 1},
]

plt.figure()
rows = int(np.ceil(np.sqrt((len(variations) * len(kernels)) / 3)))
columns = 3 * rows
for i, variation in enumerate(variations):
    for j, kernel in enumerate(kernels):
        ax = plt.subplot(rows, columns, i * len(kernels) + j + 1, projection='3d')
        ax.set(title=f'KDE: {kernel} BW: {variation["bw"]}')
        ax.scatter(xs=x, ys=y, zs=0.01)
        grid_points = 2**7
        _x, z = FFTKDE(kernel=kernel, norm=1,
                       bw=variation['bw']).fit(data)((grid_points, grid_points))
        _x, _y = np.unique(_x[:, 0]), np.unique(_x[:, 1])
        _x, _y = np.meshgrid(_x, _y)
        z = z.reshape(grid_points, grid_points).T
        ax.plot_surface(_x, _y, z, color='yellow', alpha=0.4)

weights = [
    {'name': 'exp * 25', 'weights': np.exp(data) * 25},
    {'name': '^2 * 25', 'weights': data**2 * 25},
    {'name': '/ exp * 10', 'weights': np.exp(1 / data) * 10},
]

plt.show()
exit()

plt.figure()
rows = int(np.ceil(np.sqrt(len(weights) / 2)))
columns = 2 * rows
for i, weight in enumerate(weights):
    ax = plt.subplot(rows, columns, i + 1, projection='3d')
    ax.set(title=f'KDE: Weight: {weight["name"]}')
    ax.scatter(xs=x, ys=y, zs=0.01)
    grid_points = 2 ** 7
    _x, z = FFTKDE(kernel='gaussian', norm=1,
                   bw=0.5).fit(data, weights=weight['weights'])((grid_points, grid_points))
    _x, _y = np.unique(_x[:, 0]), np.unique(_x[:, 1])
    _x, _y = np.meshgrid(_x, _y)
    z = z.reshape(grid_points, grid_points).T
    ax.plot_surface(_x, _y, z, color='yellow', alpha=0.4)





kernels = ['gaussian', 'box', 'tri']

plt.show()
exit()
