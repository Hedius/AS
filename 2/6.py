from itertools import islice, cycle

import pandas
import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import norm, gaussian_kde
from sklearn.mixture import GaussianMixture

csv = pandas.read_csv('../data_KDE_GM.csv')
data = csv.to_numpy()

# KDE
x_min = min(csv['X'])
y_min = min(csv['Y'])
x_max = max(csv['X'])
y_max = max(csv['X'])
X, Y = np.mgrid[x_min:x_max:100j, y_min:y_max:100j]
positions = np.vstack([X.ravel(), Y.ravel()])
values = np.vstack([csv['X'], csv['Y']])
kernel = gaussian_kde(values)
Z = np.reshape(kernel(positions).T, X.shape)

plt.figure()
ax = plt.subplot(3, 3, 1, projection='3d')
ax.scatter(xs=csv['X'], ys=csv['Y'], zs=0.0)
ax.plot_surface(X, Y, Z, color='y', alpha=0.8)

# b
for i in range(1, 6):
    ax = plt.subplot(3, 3, i + 1)
    mixture = GaussianMixture(n_components=i).fit(data)

    prediction = mixture.predict(data)

    # Color switching from:
    # https://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_comparison.html#sphx-glr-auto-examples-cluster-plot-cluster-comparison-py
    colors = np.array(
        list(
            islice(
                cycle(
                    [
                        "#377eb8",
                        "#ff7f00",
                        "#4daf4a",
                        "#f781bf",
                        "#a65628",
                        "#984ea3",
                        "#999999",
                        "#e41a1c",
                        "#dede00",
                    ]
                ),
                int(max(prediction) + 1),
            )
        )
    )

    plt.scatter(csv['X'], csv['Y'], s=10, color=colors[prediction])
    ax.set(xlabel='X', ylabel='Y', title=f'Components: {i}')

plt.show()
