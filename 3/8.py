import matplotlib.pyplot as plt
import numpy as np
import scipy
import scipy.stats as stats

E = 3.5
V = 2.1967

ns = [1, 2, 3, 5, 10, 20, 50, 100]
# ES MUSS DIE SAMPLE SIZE VARIIERT WERDEN


def roll():
    return stats.randint(1, 7).rvs(1)


# Für jedes X ausrechnen -> n = die sample size
# In jeder simulation n mal würfeln
# das ist der simu wert -> das plotten
def distr(samples, n):
    # sample size -> n vari
    previous = 0
    for x in samples:
        previous += ((x - E) / np.sqrt(n * V))
    return previous


def simu():
    result = roll()
    return result[0]

def control(n):
    sample = [simu() for _ in range(n)]
    s = distr(sample, n)
    values, counts = np.unique(sample, return_counts=True)
    # plt.bar(values, counts)
    plt.hist(sample, density=True)
    return np.mean(sample), s


# Nur für n = 500 machen
for n in ns:
    plt.figure()
    mean, s = control(n)

    x = np.linspace(0, 6, num=2 ** 10)
    norm = stats.norm(3, 1)
    plt.plot(x, norm.pdf(x), color='r', ls='--', label='PDF')

# 2 Plots zeichnen
# frequency distribution
# cumlative distribition

plt.show()
