import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats

"""
E = 1*0.25 + 2*0.15 + 3*0.6 = 2.349

ignore this
µ = (1 + 2 + 3) / 3 = 6 / 3 = 2

sigma^2 = (1-2.349)^2 * 0.25 + (2-2.349)^2 * 0.15 + (3-2.349)^2*0.6
sigma^2 = 0.25 + 0 + 4*0.6  = 0.727501

sigma = 0.8529
"""


n = 2000

def event():
    dist = stats.multinomial(n=1, p=[0.25, 0.15, 0.6])
    return int(np.where(dist.rvs(1) == 1)[1]) + 1


def simu():
    return event()


def control(n):
    sample = [simu() for _ in range(n)]
    values, counts = np.unique(sample, return_counts=True)
    ar = np.asarray(sample)

    expectation = np.mean(ar)
    variance = np.var(ar)
    plt.bar(values, counts)
    return expectation, variance


expectation, variance = control(n)
print(f'E: {expectation}, Var: {variance}')
plt.show()
