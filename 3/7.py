import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats

"""
Per hand:
3 Würfe
min. 2x Kopf

4 Kombinationen möglich:
HHH
HHT --> WSK hierfür
HTH
THH
() == 0:
1/4 der Events
0.25 WSK
"""


def throw():
    def _throw():
        return stats.randint(0, 2).rvs(3)

    while True:
        result = _throw()
        if result.sum() >= 2:
            return result

    # result = _throw()
    # # dirty
    # for i in range(1, 3):
    #     if result.sum() == 0:
    #         result[i] = 1
    #         continue
    #     elif result.sum() == 1 and result[i] != 1:
    #         result[i] = 1
    # return result


def simu():
    result = throw()
    # 0 is tail
    return result[2] == 0


def control(n):
    sample = [simu() for _ in range(n)]
    values, counts = np.unique(sample, return_counts=True)
    plt.bar(values, counts)
    return np.mean(sample)


print(control(10000))
plt.show()
