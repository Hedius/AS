import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import multiprocessing

p_authorized = 3.0 / 500.0
p_authorized_correctly_recognized = 0.95
p_unauthorized_access_granted = 0.01

N = 10000


def check_access():
    # check if the user is authorized or unauthorized:
    # dist = stats.poisson(p_authorized)
    # authorized = dist.rvs(1)[0]
    authorized = np.random.random() <= p_authorized
    # check access
    p = p_authorized_correctly_recognized if authorized else p_unauthorized_access_granted
    # dist = stats.poisson(p)
    # access = dist.rvs(1)[0]
    access = np.random.random() <= p
    return authorized, access


def simu(_=None):
    authorized, access = check_access()
    return (
        ('Authorized' if authorized else 'Not Authorized')
        + ' + '
        + ('Access' if access else 'No Access')
    )


def control(n):
    # with multiprocessing.Pool(8) as p:
    #    sample = p.map(simu, [_ for _ in range(n)])
    sample = [simu() for _ in range(n)]
    values, counts = np.unique(sample, return_counts=True)

    print(f'Tries: {n}\n\n')
    for value, count in zip(values, counts):
        p = count / np.sum(counts)
        print(f'{value}: p = {p}, n = {count}')

    auth_acc = sample.count('Authorized + Access')
    unauth_acc = sample.count('Not Authorized + Access')

    try:
        p = auth_acc / (auth_acc + unauth_acc)
    except ZeroDivisionError:
        p = 0
    print(f'\n\np access + authorized = {p}')
    plt.hist(sample)


control(N)
plt.show()
