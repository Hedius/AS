import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import multiprocessing


def choose(cur_id, n_persons, n_choose=2):
    chosen = []
    dist = stats.randint(0, n_persons)
    # Besser:
    # list copy
    # 1. person choices
    # # remove id
    # 2. person choice
    # ohne schleifen
    for i in range(0, n_choose):
        while True:
            temp_id = dist.rvs(1)
            if temp_id in chosen:
                continue
            if temp_id == cur_id:
                continue
            chosen.append(temp_id)
            break
    return chosen


def simu(n_persons):
    chosen_friends = []
    for i in range(0, n_persons):
        chosen_friends += choose(i, n_persons)
    alone = 0
    for i in range(0, n_persons):
        if i not in chosen_friends:
            alone += 1
    return alone


def control(n_persons, n):
    # sample = [simu(n_persons) for _ in range(n)]
    with multiprocessing.Pool(8) as p:
        sample = p.map(simu, [n_persons for _ in range(n)])
    values, counts = np.unique(sample, return_counts=True)
    ar = np.asarray(sample)

    expectation = np.mean(ar)
    variance = np.var(ar)
    plt.bar(values, counts)
    return expectation, variance


expectation, variance = control(100, 500)
print(f'E: {expectation}, Var: {variance}')
plt.show()