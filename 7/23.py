import numpy as np
import scipy.stats as stats

N = 10000

connections = [
    [115, 45],
    [70, 25]
]

c = 0.95


def p_catch_flight(t: int):
    return 1.0 / (1.0 + np.exp(-t / 10.0 + 6.0))


def get_connection_time(u, sigma):
    return u + sigma * np.random.randn()


def get_l(p, n):
    alpha = 1 - c
    return stats.norm.ppf(1 - alpha / 2) * np.sqrt(p * (1 - p) / n)


def simuflight():
    """
    simulates: single passenger doing a trip with two connections
    two connections = 3 single flights
    1. start flight 1
    2. try to catch flight 2 -> return false if missed
    3. perform flight 2
    4. try to catch flight 3  -> false if missed
    :return: True if success else False (misses a connecting flight)
    """
    for connection in connections:
        t_connection = get_connection_time(connection[0], connection[1])
        p = p_catch_flight(t_connection)
        caught = np.random.rand() <= p
        if not caught:
            return False
    return True


def control():
    samples = [simuflight() for _ in range(N)]
    p_success = samples.count(True) / len(samples)

    l = get_l(p_success, len(samples))

    print(f'N={N}')
    print(f'p_success = {p_success} ({samples.count(True)})\n')
    print(f'{round(c*100)}%-CI: [{p_success-l}, {p_success+l}] (l={l})')


if __name__ == '__main__':
    control()
