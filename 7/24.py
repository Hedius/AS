import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats

a = 10
b = 2


def myev(a, b):
    # ToDo manually find the inverse
    r = np.random.rand()
    # return a - b * np.log(np.abs(np.log(r)))
    # return np.exp(-np.exp(-(r - a) / b) * -(r - a)/b)
    return a - np.log(-np.log(r))*b


def num_a():
    samples = [myev(a, b) for _ in range(1000)]

    plt.hist(samples)


def simu():
    samples = []
    for month in range(12):
        samples.append(myev(a, b))

    count = 0
    mean = np.mean(samples)
    for sample in samples:
        if sample < mean:
            count += 1

    return 0 if count >= 6 else 1


def get_l(p, n):
    alpha = 1 - 0.95
    # Hier soll man die ungenaue formel verwenden...
    # Also nicht so wie ich mit trial und error
    return stats.norm.ppf(1 - alpha / 2) * np.sqrt(p * (1 - p) / n)


def control():
    alpha = 0.05
    l = 0.05

    # ToDo hier n ausrechnen
    # 385 muss raus kommen (ceil)

    # dann 385 mal ausführen und dann
    results = [simu()]
    while True:
        results.append(simu())

        p_good = results.count(1) / len(results)

        cur_l = get_l(p_good, len(results))

        if np.abs(cur_l - l) <= 0.001:
            break

    print(f'p_good = {p_good} ({results.count(1)}) n={len(results)}\n')
    print(f'{(1 - alpha):.1%}-CI: [{p_good - cur_l}, {p_good + cur_l}] (l={cur_l})')
    return len(results)


def main():
    num_a()
    needed_n = [control() for _ in range(100)]
    print(np.mean(needed_n))


if __name__ == '__main__':
    main()
