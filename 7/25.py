import numpy as np
import scipy.stats as stats

lag = 0.05

p_vals = [1 / 6 - lag, 1 / 6, 1 / 6, 1 / 6, 1 / 6, 1 / 6 + lag]
dist = stats.multinomial(n=1, p=p_vals)

alpha = 0.05

"""
H0: The dice is fair. Each number has the same p.
P[X=k] = 1/6 (Significance 0.05)
H1: H0 does not apply and the loaded dice is not fair anymore.
P[X=k] != 1/6 (Significance > 0.05)
"""

"""
Man sollte hier kein chi square machen....

Sondern einfach nur das p von der Zahl 6 kontrollieren.
"""


def roll_fair():
    return int(stats.randint(1, 7).rvs(1)[0])


def roll():
    result = dist.rvs(1)[0]
    return int(np.where(result == 1)[0]) + 1


def chi(samples):
    value = 0
    n = len(samples)
    for i in range(1, 7):
        f_observed = samples.count(i)
        f_real = n * (1.0 / 6.0)

        value += np.power(f_observed - f_real, 2) / f_real
    return value


def simu(n):
    samples_loaded = [roll() for _ in range(n)]
    # samples_fair = [roll_fair() for _ in range(n)]

    frequency_loaded = [samples_loaded.count(x) for x in range(1, 7)]
    frequency_fair = [n * 1.0 / 6.0 for _ in range(1, 7)]

    result = stats.chisquare(frequency_loaded, frequency_fair)
    return chi(samples_loaded), result.pvalue


def control():
    samples_loaded = [roll() for _ in range(1000)]
    print(f'p_6={samples_loaded.count(6)/len(samples_loaded)}')

    accepted = 0
    accepted_chissquare = 0
    for _ in range(1000):
        pvalue, pvalue_stats = simu(50)
        """
        https://www.statology.org/chi-square-test-by-hand/
        
        DF = 5
        11.07 critical value
        
        Bla bla bla bla bla bla :)
        """
        if pvalue < 11.07:
            accepted += 1
        if pvalue_stats >= alpha:
            accepted_chissquare += 1

    print(f'Accepted: {accepted}, Denied: {1000 - accepted}')
    print(f'Chisquare: Accepted: {accepted_chissquare}, Denied: {1000 - accepted_chissquare}')


if __name__ == '__main__':
    control()
